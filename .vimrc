nnoremap <silent> gv :vsplit<CR>gd
nnoremap <silent> g :vsplit<CR>

" Return to last edit position when opening files (You want this!)
autocmd BufReadPost *
		 \ if line("'\"") > 0 && line("'\"") <= line("$") |
		 \	 exe "normal! g`\"" |
		 \ endif

"" Set f5 to generate tags for non-latex files
nnoremap <f5> :!ctags -R<CR>

" Make searches goto the middle of the screen.
nnoremap n nzzzv
nnoremap N Nzzzv

set incsearch
set hlsearch
set autoindent

" Ctags recursive upper search
set tags=./tags;/

colorscheme torte

" Change the color scheme from a list of color scheme names.
" Version 2010-09-12 from http://vim.wikia.com/wiki/VimTip341
" Press key:
"	 F8								next scheme
"	 Shift-F8					previous scheme
"	 Alt-F8						random scheme
" Set the list of color schemes used by the above (default is 'all'):
"	 :SetColors all							(all $VIMRUNTIME/colors/*.vim)
"	 :SetColors my							 (names built into script)
"	 :SetColors blue slate ron	 (these schemes)
"	 :SetColors									(display current scheme names)
" Set the current color scheme based on time of day:
"	 :SetColors now
if v:version < 700 || exists('loaded_setcolors') || &cp
	finish
endif

let loaded_setcolors = 1
let s:mycolors = ['slate', 'torte', 'darkblue', 'delek', 'murphy', 'elflord', 'pablo', 'koehler']	" colorscheme names that we use to set color

" Set list of color scheme names that we will use, except
" argument 'now' actually changes the current color scheme.
function! s:SetColors(args)
	if len(a:args) == 0
		echo 'Current color scheme names:'
		let i = 0
		while i < len(s:mycolors)
			echo '	'.join(map(s:mycolors[i : i+4], 'printf("%-14s", v:val)'))
			let i += 5
		endwhile
	elseif a:args == 'all'
		let paths = split(globpath(&runtimepath, 'colors/*.vim'), "\n")
		let s:mycolors = uniq(sort(map(paths, 'fnamemodify(v:val, ":t:r")')))
		echo 'List of colors set from all installed color schemes'
	elseif a:args == 'my'
		let c1 = 'default elflord peachpuff desert256 breeze morning'
		let c2 = 'darkblue gothic aqua earth black_angus relaxedgreen'
		let c3 = 'darkblack freya motus impact less chocolateliquor'
		let s:mycolors = split(c1.' '.c2.' '.c3)
		echo 'List of colors set from built-in names'
	elseif a:args == 'now'
		call s:HourColor()
	else
		let s:mycolors = split(a:args)
		echo 'List of colors set from argument (space-separated names)'
	endif
endfunction

command! -nargs=* SetColors call s:SetColors('<args>')

" Set next/previous/random (how = 1/-1/0) color from our list of colors.
" The 'random' index is actually set from the current time in seconds.
" Global (no 's:') so can easily call from command line.
function! NextColor(how)
	call s:NextColor(a:how, 1)
endfunction

" Helper function for NextColor(), allows echoing of the color name to be
" disabled.
function! s:NextColor(how, echo_color)
	if len(s:mycolors) == 0
		call s:SetColors('all')
	endif
	if exists('g:colors_name')
		let current = index(s:mycolors, g:colors_name)
	else
		let current = -1
	endif
	let missing = []
	let how = a:how
	for i in range(len(s:mycolors))
		if how == 0
			let current = localtime() % len(s:mycolors)
			let how = 1	" in case random color does not exist
		else
			let current += how
			if !(0 <= current && current < len(s:mycolors))
				let current = (how>0 ? 0 : len(s:mycolors)-1)
			endif
		endif
		try
			execute 'colorscheme '.s:mycolors[current]
			break
		catch /E185:/
			call add(missing, s:mycolors[current])
		endtry
	endfor
	redraw
	if len(missing) > 0
		echo 'Error: colorscheme not found:' join(missing)
	endif
	if (a:echo_color)
		echo g:colors_name
	endif
endfunction

nnoremap <F8> :call NextColor(1)<CR>
nnoremap <S-F8> :call NextColor(-1)<CR>
nnoremap <A-F8> :call NextColor(0)<CR>

" Set color scheme according to current time of day.
function! s:HourColor()
	let hr = str2nr(strftime('%H'))
	if hr <= 3
		let i = 0
	elseif hr <= 7
		let i = 1
	elseif hr <= 14
		let i = 2
	elseif hr <= 18
		let i = 3
	else
		let i = 4
	endif
	let nowcolors = 'elflord morning desert evening pablo'
	execute 'colorscheme '.split(nowcolors)[i]
	redraw
	echo g:colors_name
endfunction

" Identated based file jump =============================================
" Jump to the next or previous line that has the same level or a lower
" level of indentation than the current line.
"
" exclusive (bool): true: Motion is exclusive
" false: Motion is inclusive
" fwd (bool): true: Go to next line
" false: Go to previous line
" lowerlevel (bool): true: Go to line with lower indentation level
" false: Go to line with the same indentation level
" skipblanks (bool): true: Skip blank lines
" false: Don't skip blank lines
function! NextIndent(exclusive, fwd, lowerlevel, skipblanks)
	let line = line('.')
	let column = col('.')
	let lastline = line('$')
	let indent = indent(line)
	let stepvalue = a:fwd ? 1 : -1
	while (line > 0 && line <= lastline)
		let line = line + stepvalue
		if ( ! a:lowerlevel && indent(line) == indent ||
					\ a:lowerlevel && indent(line) < indent)
			if (! a:skipblanks || strlen(getline(line)) > 0)
				if (a:exclusive)
					let line = line - stepvalue
				endif
				exe line
				exe "normal " column . "|"
				return
			endif
		endif
	endwhile
endfunction

" Moving back and forth between lines of same or lower indentation.
nnoremap <silent> [l :call NextIndent(0, 0, 0, 1)<CR>
nnoremap <silent> ]l :call NextIndent(0, 1, 0, 1)<CR>
nnoremap <silent> [L :call NextIndent(0, 0, 1, 1)<CR>
nnoremap <silent> ]L :call NextIndent(0, 1, 1, 1)<CR>
vnoremap <silent> [l <Esc>:call NextIndent(0, 0, 0, 1)<CR>m'gv''
vnoremap <silent> ]l <Esc>:call NextIndent(0, 1, 0, 1)<CR>m'gv''
vnoremap <silent> [L <Esc>:call NextIndent(0, 0, 1, 1)<CR>m'gv''
vnoremap <silent> ]L <Esc>:call NextIndent(0, 1, 1, 1)<CR>m'gv''
onoremap <silent> [l :call NextIndent(0, 0, 0, 1)<CR>
onoremap <silent> ]l :call NextIndent(0, 1, 0, 1)<CR>
onoremap <silent> [L :call NextIndent(1, 0, 1, 1)<CR>
onoremap <silent> ]L :call NextIndent(1, 1, 1, 1)<CR>
"====================================================================================

set mouse-=a					" Disable mouse
set noignorecase				" Enable case sensitive search
set fileformat=unix				" Force weird windows chars to appear
set ffs=unix 					" Force default line break as Unix mode
set tw=85						" Set columns length before autobreak
set backspace=indent,eol,start	" Set backspace to erase autoindent, join lines and things previous to recent typed text
"set smartindent				" Indentation method
set noexpandtab					" Tab
set tabstop=4
set shiftwidth=4
set incsearch					" Enable incremental search
set formatoptions=tcro			" Set commenting options (see help fo-table)
set ruler						" Force linecounter indicator to appear in the bottom screen
set relativenumber				" Show line number
set hidden

" Turn on filetype detection and syntax ==================================
"filetype plugin indent on

" Enable colors ==========================================================
set termguicolors " Used when dealing with gui
syntax enable
colorscheme torte

" Save last position =====================================================
	" Tell vim to remember certain things when we exit
	"'10: marks will be remembered for up to 10 previously edited files
	""100: will save up to 100 lines for each register
	":20: up to 20 lines of command-line history will be remembered
	"%: saves and restores the buffer list
	"n...: where to save the viminfo files
set viminfo='10,\"100,:20,%,n~/.viminfo
function! ResCur()
	if line("'\"") <= line("$")
		normal! g`"
		return 1
	endif
endfunction
augroup resCur
autocmd!
autocmd BufWinEnter * call ResCur()
augroup END

" =======================================================================

" Put plugins and dictionaries in this dir (also on Windows)
let vimDir = '$HOME/.vim'

if stridx(&runtimepath, expand(vimDir)) == -1
	" vimDir is not on runtimepath, add it
	let &runtimepath.=','.vimDir
endif

" Keep undo history across sessions by storing it in a file
if has('persistent_undo')
		let myUndoDir = expand(vimDir . '/undodir')
		" Create dirs
		call system('mkdir ' . vimDir)
		call system('mkdir ' . myUndoDir)
		let &undodir = myUndoDir
		set undofile
endif

" =======================================================================

" Change vim default cursor ==============================================
"let &t_SI = "\<esc>[5 q"	" blinking I-beam in insert mode
"let &t_SR = "\<esc>[3 q"	" blinking underline in replace mode
"let &t_EI = "\<esc>[ q"	" default cursor (usually blinking block) otherwise

" ========================================================================

" Save current folds =====================================================
"augroup remember_folds
"autocmd!
"let folddir = "expand(vimDir/.vim/view/"
"let branch = system(' git rev-parse --abbrev-ref HEAD | tr -d " \t\n\r"')
"let foldfile = folddir . branch . "-" . expand("%:t")
"autocmd BufWinLeave * execute "mkview! " . expand(foldfile)
"autocmd BufWinEnter * execute "silent! source" . expand(foldfile)
"augroup END

augroup remember_folds
	autocmd!
	autocmd BufWinLeave * mkview
	autocmd BufWinEnter * silent! loadview
augroup END
" ========================================================================

" Vim diff function
function! Diff(spec)
		vertical new
		setlocal bufhidden=wipe buftype=nofile nobuflisted noswapfile
				let cmd = "++edit #"
		if len(a:spec)
				let cmd = "!git -C " . shellescape(fnamemodify(finddir('.git', '.;'), ':p:h:h')) . " show " . a:spec . ":#"
		endif
		execute "read " . cmd
		silent 0d_
		diffthis
		wincmd p
		diffthis
endfunction
